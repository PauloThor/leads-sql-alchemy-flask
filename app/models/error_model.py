class EmailError(Exception):
    def __init__(self, email, *args, **kwargs):
        super().__init__(*args, email, **kwargs)

        self.message = {
            "message": f'{email} already exists.'
        }, 409        

class PhoneError(Exception):
    def __init__(self, phone, *args, **kwargs):
        super().__init__(*args, phone, **kwargs)

        self.message = {
            "message": f'{phone} already exists.'
        }, 409     

class InvalidKeysError(Exception):
    def __init__(self, keys, *args, **kwargs):
        super().__init__(keys, *args, **kwargs)

        self.message = {
            "available_keys": [
                "name",
                "email",
                "phone"
            ],
            "wrong_keys_sended": keys
        }, 422    

class InvalidValuesError(Exception):
    def __init__(self, values, *args, **kwargs):
        super().__init__(values, *args, **kwargs)
        self.values = ', '.join([str(value) for value in values])

        self.message = {
            "message": f"{self.values} should be string."
        }, 422    

class InvalidKeyError(Exception):
    def __init__(self, keys, *args, **kwargs):
        super().__init__(keys, *args, **kwargs)

        self.message = {
            "available_keys": [
                "email"
            ],
            "wrong_keys_sended": keys
        }, 422    

class EmailMissingError(Exception):
    def __init__(self, email, *args, **kwargs):
        super().__init__(email, *args, **kwargs)

        self.message = {
            "message": f'{email} does not exist.'
        }, 409    


class PhoneFormatError(Exception):
    def __init__(self, phone, *args, **kwargs):
        super().__init__(phone, *args, **kwargs)

        self.message = {
            "message": f'{phone} must be in (xx)xxxxx-xxxx format.'
        }, 409    
    