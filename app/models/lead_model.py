from sqlalchemy import Column, Integer, String
from dataclasses import dataclass

from sqlalchemy.sql.sqltypes import DateTime

from app.configs.database import db

from datetime import datetime
@dataclass
class LeadModel(db.Model):
    id: int
    name: str
    email: str
    phone: str
    creation_date: str
    last_visit: str
    visits: int

    __tablename__ = 'leads'

    id = Column(Integer, primary_key=True)
    name = Column(String(100), nullable=False)
    email = Column(String(100), nullable=False, unique=True)
    phone = Column(String(100), nullable=False, unique=True)
    creation_date = Column(DateTime)
    last_visit = Column(DateTime)
    visits = Column(Integer, default=1)