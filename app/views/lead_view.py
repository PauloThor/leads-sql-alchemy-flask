from flask import Blueprint, request, current_app, jsonify
from app.models.lead_model import LeadModel
from app.models.error_model import EmailError, InvalidKeyError, InvalidKeysError, InvalidValuesError, PhoneError
from app.models.error_model import EmailMissingError, PhoneFormatError
from datetime import datetime, timedelta
from app.controllers.lead_controller import Lead


bp = Blueprint('leads', __name__, url_prefix='/api')

@bp.post('/lead')
def create():
    data = request.get_json()
    
    try:
        lead_email = data['email']
        lead_phone = data['phone']
        lead_name = data['name']

        Lead.check_email_unique(lead_email)
        Lead.check_phone_unique(lead_phone)
        Lead.check_phone_format(lead_phone)
        Lead.check_keys(data)

        new_lead = LeadModel(
            name=lead_name,
            email=lead_email,
            phone=lead_phone,
            creation_date=datetime.now(),
            last_visit=datetime.now()
        )

        session = current_app.db.session

        session.add(new_lead)
        session.commit()

        return jsonify(new_lead), 201
    
    except EmailError as err:
        return err.message
    
    except PhoneError as err:
        return err.message

    except InvalidKeysError as err:
        return err.message
    
    except InvalidValuesError as err:
        return err.message
    
    except PhoneFormatError as err:
        return err.message


@bp.get('/lead')
def get_leads():
    query = LeadModel.query.all()

    return jsonify(query), 200


@bp.patch('/lead')
def update():
    data = request.get_json()

    try:
        Lead.check_email_key(data)
        Lead.verify_email_exists(data['email'])

        lead = LeadModel.query.filter_by(email = data['email']).first()
        
        lead.visits += 1
        lead.last_visit = datetime.now()
        
        session = current_app.db.session

        session.add(lead)
        session.commit()

        return jsonify(lead), 200

    except InvalidKeyError as err:
        return err.message
    
    except EmailMissingError as err:
        return err.message


@bp.delete('/lead')
def delete():
    data = request.get_json()

    try:
        Lead.check_email_key(data)
        Lead.verify_email_exists(data['email'])

        lead = LeadModel.query.filter_by(email = data['email']).first()
        
        session = current_app.db.session
        session.delete(lead)
        session.commit()

        return '', 204
    
    except InvalidKeyError as err:
        return err.message
    
    except EmailMissingError as err:
        return err.message
