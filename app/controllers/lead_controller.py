from app.models.lead_model import LeadModel
from app.models.error_model import EmailError, InvalidKeysError, InvalidValuesError, PhoneError, InvalidKeyError
from app.models.error_model import EmailMissingError, PhoneFormatError
import re

class Lead:
        def __init__(self):
          ...

        @staticmethod
        def check_email_unique(email: str):
            query = LeadModel.query.all()
            filtered_query = [item for item in query if item.email == email]

            if len(filtered_query) > 0:
                raise EmailError(email)


        @staticmethod
        def check_phone_unique(phone: str):
            query = LeadModel.query.all()
            filtered_query = [item for item in query if item.phone == phone]

            if len(filtered_query) > 0:
                raise PhoneError(phone)


        @staticmethod
        def check_phone_format(phone: str):
            if not re.fullmatch('\(\d{2}\)\d{5}\-\d{4}', phone):
              raise PhoneFormatError(phone)
            
        
        @staticmethod
        def check_keys(data):
          keys = data.keys()
          values = data.values()
          valid_keys = ['name', 'email', 'phone']

          incorrect_keys = [key for key in keys if key not in valid_keys]

          if len(incorrect_keys) > 0:
            raise InvalidKeysError(incorrect_keys)
          
          incorrect_values = [value for value in values if type(value) != str]
          
          if len(incorrect_values) > 0:
            raise InvalidValuesError(incorrect_values)

          
        @staticmethod
        def check_email_key(data):
          keys = data.keys()
          values = data.values()
          valid_keys = ['email']

          incorrect_keys = [key for key in keys if key not in valid_keys]

          if len(incorrect_keys) > 0:
            raise InvalidKeyError(incorrect_keys)
          
          incorrect_values = [value for value in values if type(value) != str]
          
          if len(incorrect_values) > 0:
            raise InvalidValuesError(incorrect_values)
          

        @staticmethod
        def verify_email_exists(email: str):
          query = LeadModel.query.filter_by(email = email).all()

          if len(query) < 1:
            raise EmailMissingError(email)

          